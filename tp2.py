#!/usr/bin/python3
# coding: utf-8

import yaml,io


# # @staticmethod
# def list_from_yaml(data):
#     liste = []
#     stream = io.StringIO(data)
#     l = yaml.load(stream)
#     for dico in l:
#         if dico['type']=='box':
#             liste.append(Box.from_data(dico))
#         else:
#             if dico['type']=='thing':
#                 liste.append(Thing.from_data(dico))
#     print(l)
#     return l



class Box:

    def __init__(self, op=True, capacite=None):
        self._open = op
        self._capacite = capacite
        self._contents = []

    def add(self, truc):
        self._contents.append(truc)

    def __contains__(self, machin):
        return machin in self._contents

    def remove(self, machin):
        self._contents.remove(machin)

    def is_open(self):
        return self._open

    def close(self):
        self._open = False

    def open(self):
        self._open = True

    def action_look(self):
        res = ""
        if not self._open:
            res = "la boite est fermee"
        else:
            res = "la boite contient: " + ", ".join(self._contents)
        return res

    def set_capacity(self, volume):
        self._capacite = volume

    def capacity(self):
        return self._capacite

    def has_room_for(self, thing):
        if self.capacity() == None:
            return True
        else:
            if self._capacite >= thing.volume():
                return True
            else:
                return False

    def action_add(self, thing):
        if not self.is_open():
            return False
        else:
            if self.has_room_for(thing):
                self.set_capacity(self.capacity()-thing.volume())
                self.add(thing)
                return True
        return False

    # @staticmethod
    # def from_data(data):
    #     op = data.get("is_open", None)
    #     capacite = data.get("capacity", None)
    #     return Box(op,capacite)


class Thing:

    def __init__(self, taille=0, name=None):
        self._taille = taille
        self._name = name

    def volume(self):
        return self._taille

    def set_name(self, nom):
        self._name = nom

    def __repr__(self):
        return self._name

    def has_name(self, nom):
        return self._name == nom
