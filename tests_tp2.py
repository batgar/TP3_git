#!/usr/bin/python3
# coding: utf-8

from tp2 import *
import yaml
import io

#on veut pouvoir creer des boites

def  test_box_create():
    b = Box()

#on veut pouvoir mettre des trucs dedans

def test_box_add():
    b = Box()
    b.add("truc 1")
    b.add("truc 2")

#on veut pouvoir tester qu'un truc soit dans la boite

def test_box_in():
    b = Box()
    b.add("truc1")
    assert "truc1" in b
    assert not "truc2" in b

#on veut pouvoir supprimer un truc de la boite

def test_box_remove():
    b = Box()
    b.add("truc1")
    b.add("truc2")
    b.remove("truc1")
    assert not "truc1" in b
    b.add("truc3")
    b.remove("truc2")
    assert not "truc2" in b

#on veut  savoir si la boite est ouverte

def test_box_is_open():
    b = Box()
    assert b.is_open()
    b.close()
    assert not b.is_open()
    b.open()
    assert b.is_open()

#on veut regarder ce qu'il y a dans la boite

def test_box_look():
    b = Box()
    b.add("truc1")
    assert b.action_look() == "la boite contient: truc1"
    b.close()
    assert b.action_look() == "la boite est fermee"
    b.open()
    b.add("truc2")
    assert b.action_look() == "la boite contient: truc1, truc2"

#on veut créer un objet ayant un volume

def test_thing_create():
    t = Thing(3)

#on veut connaitre le volume de notre objet

def test_thing_volume():
    t = Thing(5)
    assert t.volume() == 5

#on veut connaitre la capacité de notre boite

def test_box_capacity():
    b = Box()
    b.set_capacity(5)
    assert b.capacity() == 5
    b.set_capacity(6356)
    assert b.capacity() == 6356

#on veut savoir si la boite a assez de place pour avoir un nouvel objet

def test_box_has_room_for():
    b = Box()
    b.set_capacity(5)
    t1 = Thing(3)
    t2 = Thing(6)
    t3 = Thing(3)
    assert not b.has_room_for(t2)
    assert b.has_room_for(t1)

# on veut ajouter un objet à la boite

def test_box_action_add():
    b = Box()
    b.set_capacity(5)
    t1 = Thing(3)
    t2 = Thing(6)
    t3 = Thing(3)
    assert not b.action_add(t2)
    assert b.action_add(t1)
    assert not b.action_add(t3)
    c = Box()
    c.close()
    assert not c.action_add(t1)

# on veut nommer un objet

def test_thing_set_name():
    t1 = Thing()
    t1.set_name("Bidule")
    assert repr(t1) == "Bidule"

# on veut savoir si un objet a un nom donné

def test_thing_has_name():
    t1 = Thing()
    t1.set_name("Bidule")
    assert t1.has_name("Bidule")
    t3 = Thing()
    assert not t3.has_name("1")

# def test_box_find():
#     b = Box()
#     t1 = Thing()
#     t1.set_name("bidule")
#     b.add(t1)
#     assert b.find("bidule")


# Exo 8

# Boite depuis YAML

def test_list_from_yaml():
    text = """
    - type: box
      is_open: True
      capacity: 3
    - type: box
      is_open: False
      capacity: 9001
    """
    l= list_from_yaml(text)
    assert l[0].capacity() == 3
    assert l[0].is_open()
    assert l[1].capacity() == 9001
    assert not l[1].is_open()
